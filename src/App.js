// import logo from "./logo.svg";
import "./App.css";
import ReactRedux from "./ReactRedux/ReactRedux";

function App() {
  return (
    <div className="App">
      <ReactRedux />
    </div>
  );
}

export default App;
