import React, { Component } from "react";
import { connect } from "react-redux";
import { change_value } from "./redux/actions/numberAction";

class ReactRedux extends Component {
  render() {
    return (
      <div className="text-center">
        <button
          onClick={() => {
            this.props.changeValue(1);
          }}
          className="btn btn-success mr-4"
        >
          +
        </button>
        <span className="display-4">{this.props.soLuong}</span>
        <button
          onClick={() => {
            this.props.changeValue(-1);
          }}
          className="btn btn-danger ml-4"
        >
          -
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    changeValue: (value) => {
      dispatch(change_value(value));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReactRedux);
