import { CHANGE_VALUE } from "../constants/numberConstant";

export const change_value = (value) => {
  return {
    type: CHANGE_VALUE,
    payload: value,
  };
};
