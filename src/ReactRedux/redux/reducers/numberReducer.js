import { CHANGE_VALUE } from "../constants/numberConstant";

let initialState = {
  number: 10,
};

export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_VALUE: {
      state.number += action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
